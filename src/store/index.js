import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth';
import filiales from './filiales';
import employes from './employes';
import childs from './childs';
import directors from './directors';

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      filiales,
      employes,
      childs,
      directors
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
