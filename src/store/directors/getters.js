export function directors (state) {
    return state.directors;
}

export default function() {
    return {
        directors,
    }
}
