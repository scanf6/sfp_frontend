import services from '../../services';

export function getDirectors (context, options) {
    return new Promise((resolve, reject) =>
        services.directors
          .all(options)
          .then(({ data }) => {
            context.commit("SETDIRECTORS", data);
            resolve(data);
          })
          .catch((error) => reject(error))
    );
}

export function createDirector (context, director) {
    return new Promise((resolve, reject) =>
        services.directors
          .create(director)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function deleteDirector (context, id) {
    return new Promise((resolve, reject) =>
        services.directors
          .remove(id)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function modifyDirector (context, { id, data }) {
    return new Promise((resolve, reject) =>
        services.directors
          .put(id, data)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export default function() {
    return {
        getDirectors,
        createDirector,
        deleteDirector,
        modifyDirector
    }
}