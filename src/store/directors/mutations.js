export function SETDIRECTORS (state, directors) {
    state.directors = directors;
}

export default function() {
    return {
        SETDIRECTORS,
    }
}