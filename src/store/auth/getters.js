export function isLogged (state) {
    return state.isLogged;
}

export function loggedAs (state) {
    return state.loggedAs;
}

export default function() {
    return {
        isLogged,
        loggedAs
    }
}
