export function SETLOGGED (state, logged) {
    state.isLogged = logged;
}

export function SETLOGGEDAS (state, user) {
    state.loggedAs = user;
}

export default function() {
    return {
        SETLOGGED,
        SETLOGGEDAS
    }
}