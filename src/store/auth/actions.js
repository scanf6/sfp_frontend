import services from '../../services';

export function login (context, payload) {
    return new Promise((resolve, reject) =>
        services.auth
          .signin(payload)
          .then(({ data }) => {
            localStorage.setItem("authoken", JSON.stringify(data.token));
            localStorage.setItem("authId", JSON.stringify(data.userId));
            context.dispatch("profile", data.userId)
                .then((result) => {
                  context.commit("SETLOGGED", true);
                  context.commit("SETLOGGEDAS", result);
                  resolve(result);
                });
          })
          .catch((error) => reject(error))
    );
}

export function logout (context) {
    return new Promise((resolve, reject) => {
      localStorage.removeItem("authoken");
      localStorage.removeItem("authId");
      resolve(true);
    });
}

export function profile (context, id) {
    return new Promise((resolve, reject) =>
        services.auth
          .profile(id)
          .then(({ data }) => {
            context.commit("SETLOGGEDAS", data);
            context.commit("SETLOGGED", true);
            resolve(data);
          })
          .catch((error) => reject(error))
    );
}

export default function() {
    return {
        login,
        logout
    }
}