import services from '../../services';

export function getEmployes (context, options) {
    return new Promise((resolve, reject) =>
        services.employes
          .all(options)
          .then(({ data }) => {
            context.commit("SETEMPLOYES", data);
            resolve(data);
          })
          .catch((error) => reject(error))
    );
}

export function createEmploye (context, employe) {
    return new Promise((resolve, reject) =>
        services.employes
          .create(employe)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function removeEmploye (context, id) {
    return new Promise((resolve, reject) =>
        services.employes
          .remove(id)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function modifyEmploye (context, { id, data }) {
    return new Promise((resolve, reject) =>
        services.employes
          .put(id, data)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export default function() {
    return {
        getEmployes,
        createEmploye,
        removeEmploye,
        modifyEmploye
    }
}