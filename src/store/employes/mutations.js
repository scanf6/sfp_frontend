export function SETEMPLOYES (state, employes) {
    state.employes = employes;
}

export default function() {
    return {
        SETEMPLOYES,
    }
}