export function SETFILIALES (state, filiales) {
    state.filiales = filiales;
}

export default function() {
    return {
        SETFILIALES,
    }
}