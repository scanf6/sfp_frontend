import services from '../../services';

export function getFiliales (context, options) {
    return new Promise((resolve, reject) =>
        services.filiales
          .all(options)
          .then(({ data }) => {
            context.commit("SETFILIALES", data);
            resolve(data);
          })
          .catch((error) => reject(error))
    );
}

export function createFiliale (context, filiale) {
    return new Promise((resolve, reject) =>
        services.filiales
          .create(filiale)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function deleteFiliale (context, id) {
    return new Promise((resolve, reject) =>
        services.filiales
          .remove(id)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function modifyFiliale (context, { id, data }) {
    return new Promise((resolve, reject) =>
        services.filiales
          .put(id, data)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export default function() {
    return {
        getFiliales,
        createFiliale,
        deleteFiliale,
        modifyFiliale
    }
}