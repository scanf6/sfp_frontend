export function SETCHILDS (state, childs) {
    state.childs = childs;
}

export default function() {
    return {
        SETCHILDS,
    }
}