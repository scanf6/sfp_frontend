import services from '../../services';

export function getChilds (context, options) {
    return new Promise((resolve, reject) =>
        services.childs
          .all(options)
          .then(({ data }) => {
            context.commit("SETCHILDS", data);
            resolve(data);
          })
          .catch((error) => reject(error))
    );
}

export function createChild (context, child) {
    return new Promise((resolve, reject) =>
        services.childs
          .create(child)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function removeChild (context, id) {
    return new Promise((resolve, reject) =>
        services.childs
          .remove(id)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export function modifyChild (context, { id, data }) {
    return new Promise((resolve, reject) =>
        services.childs
          .put(id, data)
          .then(() => {
            resolve(true);
          })
          .catch((error) => reject(error))
    );
}

export default function() {
    return {
        getChilds,
        createChild,
        removeChild,
        modifyChild
    }
}