import { date } from 'quasar';

export default ({ Vue}) => {
    Vue.filter('plainDate', function (value) {
        if (!value) return ''
        return date.formatDate(value, 'DD-MM-YYYY');
    })
}