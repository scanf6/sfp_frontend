
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', redirect: '/filiales' },
      { path: '/filiales', name: 'filiales', component: () => import('pages/Filiales.vue') },
      { path: '/employes', name: 'employes', component: () => import('pages/Employes.vue') },
      { path: '/enfants', name: 'enfants', component: () => import('pages/Enfants.vue') },
      { path: '/directors', name: 'directors', component: () => import('pages/Directeurs.vue') },
      { path: '/profile', name: 'profile', component: () => import('pages/Profile.vue') },
    ]
  },
  {
    path: '/login',
    component: () => import('pages/Login.vue')
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
