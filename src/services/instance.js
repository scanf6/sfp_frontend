import axios from "axios";

const env  = process.env.NODE_ENV;

const _axios = axios.create({
  baseURL: (!env || env === 'development') ?
  "http://localhost:5000/api":
  "/api"
});

// List of excpetion for the JWT token
const exceptRoutes = [
  "/auth/login",
];

_axios.interceptors.request.use(
  config => {
    const token = JSON.parse(localStorage.getItem("authoken"));
      if (
          config.method === "get" &&
          exceptRoutes.indexOf(config.url.split("?")[0]) !== -1
      ) return config;
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  error => Promise.reject(error)
);

export default _axios;