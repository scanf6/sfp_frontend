import instance from "./instance";

export const signin = async data => {
    return await instance.post('/auth/login', data);
}


export const profile = async (id) => {
  return await instance.get(`/auth/profile/${id}/`);
};