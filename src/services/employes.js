import _ from 'lodash';
import instance from "./instance";

export const create = async data => {
    return await instance.post('/employes', data);
}

export const remove = async id => {
    return await instance.delete(`/employes/${id}`);
}

export const put = async (id, data) => {
    return await instance.put(`/employes/${id}`, data);
}

export const all = async (options = { filter: "", filterByFiliale: 0, page: 1, rowsPerPage: 5}) => {
  let requestQuery = "";
  let params = _.map(options, (value, key) => `${key}=${value}`);
  requestQuery = params.join("&");
  return await instance.get(`/employes?${requestQuery}`);
};