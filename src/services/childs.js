import _ from 'lodash';
import instance from "./instance";

export const create = async data => {
    return await instance.post('/childs', data);
}

export const remove = async id => {
    return await instance.delete(`/childs/${id}`);
}

export const put = async (id, data) => {
    return await instance.put(`/childs/${id}`, data);
}

export const all = async (options = { filter: "", page: 1, rowsPerPage: 5}) => {
  let requestQuery = "";
  let params = _.map(options, (value, key) => `${key}=${value}`);
  requestQuery = params.join("&");
  return await instance.get(`/childs?${requestQuery}`);
};