import * as auth from './auth';
import * as filiales from './filiales';
import * as employes from './employes';
import * as childs from './childs';
import * as directors from './directors';

export default {
    auth,
    filiales,
    employes,
    childs,
    directors
}